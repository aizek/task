class Assignment < ActiveRecord::Base
  belongs_to :user
  belongs_to :meeting
  validates :meeting_id, uniqueness: { scope: :user_id }			# add validation of uniqueness in the model and in db as unique index
end