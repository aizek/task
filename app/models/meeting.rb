require 'ostruct'

class Meeting < ActiveRecord::Base
  date_time_attribute :started_at
  validates :started_at, presence: true 
  validates :name, presence: true


  SELECT_FIELDS = 'users.*, assignments.role as role'
  
  has_many :assignments, dependent: :destroy
  has_many :participants, -> { select(SELECT_FIELDS) }, through: :assignments, source: :user

  has_many :presenters, -> { select(SELECT_FIELDS).where("role = 1") }, 
    through: :assignments, 
    source: :user
  
  has_many :listeners, -> { select(SELECT_FIELDS).where("role = 0") }, 
    through: :assignments, 
    source: :user
  

  def self.fast_hashs( relation )
    select_res = connection.select_all( relation.arel )
    select_res.map { |attributes|
      attributes.each_key { | column_name |
        attributes[ column_name ] = column_types.fetch(column_name).type_cast attributes[ column_name ]
      }
      OpenStruct.new( attributes )
    }
  end
    
  # Build association
  def add_user(user, role = 0)
    self.assignments.build(user_id: user_id(user), role: role)
  end

  # Create association
  def add_user!(user, role = 0)
    self.assignments.create!(user_id: user_id(user), role: role)
  end

  private
    def user_id(user)
      user.is_a?(User) ? user.id : user
    end

end
