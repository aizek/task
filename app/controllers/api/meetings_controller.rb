module Api
  class MeetingsController < Api::ApplicationController
    
    before_action :set_meeting, only: [:show, :destroy, :update]


    # as generally the most used function in the app, 'index' action needs to be boosted
    def index
      @meetings = Meeting.paginate(page_params).order(order).select([:id, :name, :started_at])      # don't load all fields
      @meetings = @meetings.where("name ilike :q", q: "%#{ params[:q]}%" ) if params[:q].present?
      @meetings_hash = Meeting.fast_hashs @meetings     # optional: fast array of hashes instead of AR relation (x2 performance)
                                                        # down: can't use will_paginates's extended methods on that
    end

    def show
      @users = @meeting.participants
    end

    def create
      @meeting = Meeting.new meeting_params
      if !@meeting.save
        render :status => :forbidden, :text => "input_error"
        return
      end
      create_participants
      render 'show'
    end

    def update
      render 'show'
    end

    def destroy
      @meeting.destroy
      render 'show'
    end

    def default_order
      'started_at DESC'
    end

    private
      def create_participants
        return if not participants_params
        participants_params.each { |p| @meeting.add_user(p[:id],p[:role]) }
        @meeting.save
      end

      def participants_params
        params.require(:participants).permit(:id,:role)           # strong params is stronger
      end

      def set_meeting
        @meeting = Meeting.find_by_id(params[:id])
        if @meeting.nil?
          render :status => :not_found, :text => 'not_found' 
          return
        end
      end

      def meeting_params
        params.require(:meeting).permit(:id,:name,:started_at,:started_at_date,:started_at_time)
      end
  end
end