json.meeting do
  json.id @meeting.id 
  json.started_at_utc @meeting.started_at.to_i
  json.name @meeting.name
  json.start_time @meeting.started_at.strftime('%d %B %I:%M %P')
  json.url "#{request.protocol}#{request.host_with_port}/meetings/#{@meeting.id}"	# this stuff can be generated on client-side
end

# invoke partial from iterator - not the fastest thing in jbuilder: refactor
json.participants do
  json.partial! 'api/shared/participants', users: @users
end