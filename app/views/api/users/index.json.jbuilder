# invoke partial from iterator - not the fastest thing in builder: refactor
json.users do
	json.partial! 'api/users/users', users: @users
end

json.partial! 'api/shared/pagination', items: @users