namespace :populate do
  desc "Populate DB with data"

  task users: :environment do
    count = 1000
    count.times do # more idiomatic
      User.create(
        name: Faker::Name.first_name, 
        email: Faker::Internet.email, 
        password: "qwerty", 
        password_confirmation: "qwerty", 
        gender: rand(2), # I think, rand would be enough
        age: rand(99) )
    end
  end

  task meetings: :environment do
    count = 300
    now = Time.zone.now
    puts "populate meetings #{now} #{count}"
    all_users = User.all   # to sample random users on the database-side is too costly, so cache them and sample locally
    count.times do # more 'rubyish'
      m = Meeting.create(name: Faker::Company.catch_phrase,  started_at: now+60*60*(Faker::Number.number(2).to_i - 300))
      # no need to 'save' after 'create'
      all_users.sample(Faker::Number.number(3).to_i).each do |u|
        m.add_user! u, rand(2) # I think, rand would be enough
      end
    end 
  end

end
