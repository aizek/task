class AddPerformanceBoostIndecies < ActiveRecord::Migration
	def change
		add_index :assignments, :meeting_id
		add_index :assignments, :user_id
		add_index :assignments, [:meeting_id, :user_id]
		add_index :assignments, [:user_id, :meeting_id], unique: true
	end
end
